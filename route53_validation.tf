resource "aws_route53_record" "cert_validation_api" {
  name     = var.api_acm_resource_record_name
  type     = var.acm_api_resource_record_type
  zone_id  = var.zone_id
  records  = [var.acm_api_resource_record_value ]
  ttl      = 60
}
output "api_route53_fqdn" {
  value = aws_route53_record.cert_validation_api.fqdn
}